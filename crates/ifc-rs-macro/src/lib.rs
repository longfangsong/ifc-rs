#![feature(box_patterns)]
extern crate proc_macro;

use proc_macro::{Span, TokenStream};
use quote::quote;
use syn::parse_macro_input;
use syn::punctuated::Punctuated;
use syn::token::{Brace, *};
use syn::*;

#[proc_macro]
pub fn check_side_effect(input: TokenStream) -> TokenStream {
    // Parse the input tokens into a syntax tree
    let input = parse_macro_input!(input as Expr);
    let result = if let Expr::Closure(mut closure) = input {
        let params: Vec<_> = closure
            .inputs
            .iter()
            .map(|param| {
                let param_pat = if let Pat::Type(PatType { pat, .. }) = param {
                    pat.as_ref()
                } else {
                    param
                };
                if let Pat::Ident(PatIdent { ident, .. }) = param_pat {
                    ident
                } else {
                    unreachable!("Expect an ident");
                }
            })
            .collect();
        // dbg!(params);
        let mut body = if let box Expr::Block(block) = closure.body {
            block
        } else {
            ExprBlock {
                attrs: vec![],
                label: None,
                block: Block {
                    brace_token: Brace::default(),
                    stmts: vec![Stmt::Expr(*closure.body, None)],
                },
            }
        };
        let stmts = visit_closure_body(&body);
        body.block.stmts = stmts;
        closure.body = std::boxed::Box::new(Expr::Block(body));
        quote! { unsafe { SideEffectFreeFn::new(#closure) } }
    } else {
        quote! { compile_error!("A closure is expected"); }
    };
    result.into()
}

fn visit_closure_body(block: &ExprBlock) -> Vec<Stmt> {
    let mut locals = vec![vec![]];
    let mut externs = Vec::new();
    let mut result = Vec::new();
    for stmt in &block.block.stmts {
        match stmt {
            Stmt::Local(local) => {
                let mut local_to_check = None;
                if let Pat::Ident(ident) = &local.pat {
                    locals.last_mut().unwrap().push(ident.ident.clone());
                    local_to_check = Some(ident);
                } else {
                    todo!()
                }
                if let Some(LocalInit { expr, .. }) = &local.init {
                    result.extend(visit_expr(&mut locals, &mut externs, expr.as_ref()));
                }
                result.push(Stmt::Local(local.clone()));
                if let Some(local_to_check) = local_to_check {
                    result.push(Stmt::Expr(
                        Expr::MethodCall(ExprMethodCall {
                            attrs: vec![],
                            receiver: std::boxed::Box::new(syn::Expr::Path(ExprPath {
                                attrs: vec![],
                                qself: None,
                                path: Path {
                                    leading_colon: None,
                                    segments: [PathSegment {
                                        ident: local_to_check.ident.clone(),
                                        arguments: PathArguments::None,
                                    }]
                                    .into_iter()
                                    .collect(),
                                },
                            })),
                            dot_token: Dot::default(),
                            method: Ident::new("check", Span::call_site().into()),
                            turbofish: None,
                            paren_token: Paren::default(),
                            args: Punctuated::default(),
                        }),
                        Some(Semi::default()),
                    ));
                }
            }
            Stmt::Item(item) => {
                result.push(Stmt::Item(item.clone()));
            }
            Stmt::Expr(expr, semi) => {
                result.extend(visit_expr(&mut locals, &mut externs, expr));
                result.push(Stmt::Expr(expr.clone(), semi.clone()));
            }
            Stmt::Macro(_) => panic!("macro is not allowed here"),
        }
    }
    let mut extern_check_code: Vec<_> = externs
        .into_iter()
        .map(|it| {
            Stmt::Expr(
                Expr::MethodCall(ExprMethodCall {
                    attrs: vec![],
                    receiver: std::boxed::Box::new(syn::Expr::Path(ExprPath {
                        attrs: vec![],
                        qself: None,
                        path: Path {
                            leading_colon: None,
                            segments: [PathSegment {
                                ident: it.clone(),
                                arguments: PathArguments::None,
                            }]
                            .into_iter()
                            .collect(),
                        },
                    })),
                    dot_token: Dot::default(),
                    method: Ident::new("check", Span::call_site().into()),
                    turbofish: None,
                    paren_token: Paren::default(),
                    args: Punctuated::default(),
                }),
                Some(Semi::default()),
            )
        })
        .collect();
    extern_check_code.extend(result);
    extern_check_code
}

fn visit_expr(locals: &mut Vec<Vec<Ident>>, externs: &mut Vec<Ident>, expr: &Expr) -> Vec<Stmt> {
    let mut result = Vec::new();
    match expr {
        Expr::Binary(ExprBinary { left, right, .. }) => {
            result.extend(visit_expr(locals, externs, left.as_ref()));
            result.extend(visit_expr(locals, externs, right.as_ref()));
        }
        Expr::Assign(ExprAssign { left, right, .. }) => {
            result.extend(visit_expr(locals, externs, left.as_ref()));
            result.extend(visit_expr(locals, externs, right.as_ref()));
        }
        Expr::MethodCall(ExprMethodCall { receiver, .. }) => match receiver.as_ref() {
            Expr::Path(ExprPath { path, .. }) => {
                if let Some(ident) = path.get_ident() {
                    externs.push(ident.clone());
                }
            }
            _ => (),
        },
        Expr::Unary(ExprUnary { expr, .. }) => {
            result.extend(visit_expr(locals, externs, expr.as_ref()));
        }
        Expr::Call(ExprCall { func, args, .. }) => {
            result.extend(visit_expr(locals, externs, func.as_ref()));
            result.extend(args.iter().flat_map(|e| visit_expr(locals, externs, e)));
        }
        Expr::Lit(_) => (),
        Expr::Block(ExprBlock { block, .. }) => {
            for stmt in &block.stmts {
                result.extend(visit_stmt(locals, externs, stmt));
            }
        }
        Expr::ForLoop(ExprForLoop { expr, body, .. }) => {
            result.extend(visit_expr(locals, externs, expr.as_ref()));
            for stmt in &body.stmts {
                result.extend(visit_stmt(locals, externs, stmt));
            }
        }
        Expr::If(ExprIf {
            cond,
            then_branch,
            else_branch,
            ..
        }) => {
            result.extend(visit_expr(locals, externs, cond.as_ref()));
            for stmt in &then_branch.stmts {
                result.extend(visit_stmt(locals, externs, stmt));
            }
            if let Some((_, else_branch)) = else_branch {
                result.extend(visit_expr(locals, externs, else_branch.as_ref()));
            }
        }
        Expr::Loop(ExprLoop { body, .. }) => {
            for stmt in &body.stmts {
                result.extend(visit_stmt(locals, externs, stmt));
            }
        }
        Expr::Match(ExprMatch { expr, arms, .. }) => {
            result.extend(visit_expr(locals, externs, expr.as_ref()));
            for Arm { guard, body, .. } in arms {
                if let Some((_, expr)) = guard {
                    result.extend(visit_expr(locals, externs, expr.as_ref()));
                }
                result.extend(visit_expr(locals, externs, body.as_ref()));
            }
        }
        Expr::While(ExprWhile { cond, body, .. }) => {
            result.extend(visit_expr(locals, externs, cond.as_ref()));
            for stmt in &body.stmts {
                result.extend(visit_stmt(locals, externs, stmt));
            }
        }
        Expr::Macro(_) => panic!("Macro is not allowed here"),
        e => {
            // dbg!(&e);
        }
    }
    result
}

fn visit_stmt(locals: &mut Vec<Vec<Ident>>, externs: &mut Vec<Ident>, stmt: &Stmt) -> Vec<Stmt> {
    let mut result = Vec::new();
    match stmt {
        Stmt::Local(local) => {
            if let Pat::Ident(ident) = &local.pat {
                locals.last_mut().unwrap().push(ident.ident.clone());
                if let Some(LocalInit { expr, .. }) = &local.init {
                    result.extend(visit_expr(locals, externs, expr.as_ref()));
                }
                result.push(Stmt::Local(local.clone()));
                if let Pat::Ident(PatIdent { ident, .. }) = &local.pat {
                    vec![Stmt::Expr(
                        Expr::MethodCall(ExprMethodCall {
                            attrs: vec![],
                            receiver: std::boxed::Box::new(syn::Expr::Path(ExprPath {
                                attrs: vec![],
                                qself: None,
                                path: Path {
                                    leading_colon: None,
                                    segments: [PathSegment {
                                        ident: ident.clone(),
                                        arguments: PathArguments::None,
                                    }]
                                    .into_iter()
                                    .collect(),
                                },
                            })),
                            dot_token: Dot::default(),
                            method: Ident::new("check", Span::call_site().into()),
                            turbofish: None,
                            paren_token: Paren::default(),
                            args: Punctuated::default(),
                        }),
                        Some(Semi::default()),
                    )]
                } else {
                    vec![]
                }
            } else {
                todo!()
            }
        }
        Stmt::Expr(expr, _) => visit_expr(locals, externs, expr),
        Stmt::Macro(_) => panic!("No macros allowed here!"),
        Stmt::Item(_) => vec![],
    }
}

#[cfg(test)]
mod tests {
    use super::*;
}
