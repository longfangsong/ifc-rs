---
---
We believe that everyone who attend this course has already familiar what is information flow control, basically it prevents information flows from high security level to low security level.
---
And maybe some of you know Rust language, which is a strong statically typed language. And provides memory safety.
---
What we want to implement is an IFC library in rust, which focus on easy of use, so that it can be adopted progressively. And we hope all checks can be implemented in compile time.
---
To implement the library, we first need to provide the user a way to define a security level lattice.

As many IFC library does, we give each secret variable a label.

And we believe only a part of the variables needs to be protected, so we put the untagged variables at the bottom of the lattice.

---
We use traits to implement it.

Basically it just define the relationship between security level tags.

For user code we just define the tags and impl traits for then, and a lattice is formed.
---
Next thing we do is to add a `SecretCell` **functor**, technically we can make it a **monad** (and we did in our code) but in most cases we just use it as a functor.
---
With the `map` method, we can apply some function into the value inside the cell and the result will be wrapped in a `SecretCell` in the same Security level. So given the function passed into `map` is side-effect-free, information flow can go into the cell, but not out of it.
---
Also we provide a declassify method and mark it as unsafe, so it **should** be audited when, for example, doing code review.
---
To make sure the function is side-effect-free, first we tried to mark it an `Fn` instead of an `FnMut` or `FnOnce`, which most other monad like structures in Rust does.

According to rust doc, `Fn` can be called repeatedly without mutating state. Because it captures outer variables by only immutable refrence.
---
This seems to work at the first glance, we cannot assign or modify variables outside the closure.
---
But there is a thing called interior mutability, which enables you to modify a field of some variables by immutable refrence only.
---
Moreover, `Fn` can not prevent side effects introduced by using IO.
---
So to fix this problem, we shall disallow all those things to be used in a closure.
We can do this with macro.
---
Mark types which may introduce side effects by immutable references are marked by using negative `impl` of an auto trait.

And we define a `check` method which occurs on all types except those we have already marked non-side-effect-free.
---
So if we call `check` method on a non-side-effect-free variable, the type checker will complain that there are no such method, but all other types of variables are ok.
---
And then we create a wrapper around `Fn` to represent functions that contains only these side effect free types.
We mark its constructor as `unsafe` so the user code cannot create it manually in safe rust.
---
And we update the `map` method like this.
---
Now what we want is do is just create a proc macro to add these `check` method calls automatically to the closure we passed in.
---
Though the implementation is very complex, the basic idea is simple: (read the slide).
---
So here's the final solution look like on the client side:

As you may see, we wrap the secret data in the cell, and we can map a side-effect-free closure over it, with checks.
---
And the check macro will add checks for us and make the type checker fail.

Thus, information flow won't leave the SecretCell, and our secret data is protected.
---
That's basically all what we have done.

And there remains some todos:

(read the slide).

---
And we can push the ablilty of the library even more:

(read the slide)

Since macros are so powerful we can add many checks as we want by just adding more labor work onto it.
---

And I should mention that our idea is come from this paper. (Read the slide.) Though we have some different design choices with them, this paper is still an interesting one for reading.
