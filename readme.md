---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
math: "mathjax"
style: |
  mjx-container.MathJax svg {
    max-width: 100%;
    margin: 0 auto;
  }

  img {
    max-width: 100%;
    max-height: 350px;
  }

  p, li {
    font-size: 40px;
  }

  pre, code {
    font-size: 30pt;
  }
---

# IFC-rs

Group 42
Fangdelong Su
Fangsong Long

---

## Background

### Information flow control

![1](readme/1.png)

---

### Rust

- Strong statically typed (quite powerful)
- Memory safety

---

### Goal

Implement an IFC library in rust.

- Compile time check
- Easy of use, can be progressively adopted

---

## Implementation

### Security level Lattice

![2](readme/2.png)

---

### In code

```rust
pub trait LessOrEqualSecretThan<L> {}
pub trait MoreOrEqualSecretThan<L> {}

// reflexitivity of the label relationship
impl<L> MoreOrEqualSecretThan<L> for L {}
// for label L1 and L2, L2 <= L1 means L1 >= L2
impl<L1, L2> LessOrEqualSecretThan<L1> for L2 where L1: MoreOrEqualSecretThan<L2> {}
```

```rust
struct L1 {}
struct L2 {}
struct L1_L2 {}
impl MoreOrEqualSecretThan<L1> for L1_L2 {}
impl MoreOrEqualSecretThan<L2> for L1_L2 {}
```

---

### `SecretCell` Functor

![width:500px](readme/3.svg)

---

### `SecretCell::map`

![width:1024px](readme/4.svg)

So information flow can go into the cell, but not out of it.

---

### `SecretCell::declassify`

![width:1024px](readme/5.svg)

Mark as `unsafe` means this needs to be audited.

---

## Side effect free

### First naïve approach

```rust
pub fn map<T2, F: Fn(T1) -> T2>(self, f: F) -> SecretCell<T2, L1> {...}
```

Note the usage of `fn`, according to rust doc:

> Instances of Fn can be called repeatedly without mutating state.

---

### Seems it works...

```rust
let mut secret_data = SecretCell<_, LabelSecretLevelTop>::new("Hello".to_string());
let mut unsecure_data = String::new();
let new_secret_data = secret_data.map(|sec_data| {
  unsecure_data = sec_data;
});
```

Won't type check. ie. we prevent information in `secret_data` go out of the closure by assign to `unsecure_data`.

---

### Problem — interior mutability

```rust
let mut secret_data = SecretCell<_, LabelSecretLevelTop>::new("Hello".to_string());
let mut unsecure_data = String::new();
let unsecure_cell = RefCell::new(unsecure_data);
let new_secret_data = secret_data.map(|sec_data| {
  *unsecure_cell.borrow_mut() = sec_data;
});
let new_unsecure_data = unsecure_cell.borrow().clone();
// new_unsecure_data now contains the content of secret_data!
```

Leaked!

---

### Problem — IO

```rust
let mut secret_data = SecretCell<_, LabelSecretLevelTop>::new("Hello".to_string());
let new_secret_data = secret_data.map(|sec_data| {
  println!("{sec_data}");
});
```

Also leaked!

---

### Fix

Except for just using `Fn`, we add one more requirement:

- No variable in interior mutability types or IO types can be used in the closure.

We can achieve this with help of macro.

---

#### Marking disallowed types

##### Conservative approach with negative impl

Most types are OK except ...

```rust
pub unsafe auto trait SideEffectFree {}
impl<T> !SideEffectFree for std::cell::UnsafeCell<T> {}
impl !SideEffectFree for std::fs::File {}
impl !SideEffectFree for std::net::UdpSocket {}
impl !SideEffectFree for std::net::TcpStream {}
pub unsafe trait SideEffectChecker { fn check(self) {} }
unsafe impl<T: SideEffectFree> SideEffectChecker for T {}
```

And you can mark your own defined structs.

---

```rust
let f = File::new(...);
f.check(); // <- Won't type check

let i: i32 = 0;
i.check(); // <- This will pass!
```

<!-- So if a type is `SideEffectFree`, it will have a `check()` method, or we can say:

**If we call `a.check()` on `a` and `a`'s type is marked as not side effect free, the complier would complain that `a` doesn't have such a method.** -->

---

#### Wrapping mapped function types

We create a wrapper struct over `Fn` to mark an side effect free function.

```rs
pub struct SideEffectFreeFn<T1, T2, F: Fn(T1) -> T2> {...}

impl<T1, T2, F: Fn(T1) -> T2> SideEffectFreeFn<T1, T2, F> {
  // mark as unsafe to prevent the user create it accidentally
  pub unsafe fn new(f: F) -> Self {...}
}
```

---

```diff
impl<T1, L1> SecretCell<T1, L1> {
  // ...
-   pub fn map<T2, F: Fn(T1) -> T2>(self, f: F) -> SecretCell<T2, L1>;
+   pub fn map<T2, F: Fn(T1) -> T2>(self, f: SideEffectFreeFn<T1, T2, F>) -> SecretCell<T2, L1>;
  // ...
}
```

---

##### Proc Macro for checking and construction `SideEffectFreeFn`

The basic idea is:

- Walk along the AST of a closure to collect all variables used
- For each variable, if it is defined inside the closure, check directly after define
- If it is not, add check code at the head of th closure.
- We do not allow any macro calls inside —— don't know what will it be expanded to
- Wrap the result code into `SideEffectFreeFn`

---

### How does the final solution look like?

```rs
let mut secret_data = SecretCell<_, LabelSecretLevelTop>::new("Hello".to_string());

let unsecure_cell = RefCell::new(0);
let new_secret_data = secret_data.map(check_side_effect!(
    |sec_data| {
        *unsecure_cell.borrow_mut() = sec_data;
        let mut f = File::new("...");
        f.write_all("abc");
    }
));
```

---

#### Expanded to

```rs
let new_secret_data = secret_data.map(unsafe {
    SideEffectFreeFn::new(|sec_data| {
        unsecure_cell.check();
        *unsecure_cell.borrow_mut() = sec_data;
        let mut f = File::new("...");
        f.check();
    })
});
```

And won't type check because `unsecure_cell`  and `f` do not have a `check` method.

---

## Remain problems & future work

- Fuctions can create an unsecure type variable from nowhere and consume it at once, eg. `io::stdout().write_all("");`
  - Maybe add `.check()` after each function call.

---

- Macro can actually help more, eg. we can expand the content of each `check_side_effect!` to something like

  ```rs
  let start_time = Time::now();
  // real work
  sleep(WCET-(Time::now() - start_time));
  ```

  Which can somehow prevent time attack.

---

## References & Thanks

- Cocoon: Static Information Flow Control in Rust
  - Do not make full use of a monad like structure, fully depend on macro
  - "progressive" or "white list", check function calls
    - require specify each function's full path, eg. `::std::string::String::len`

