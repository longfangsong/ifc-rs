#![feature(auto_traits)]
#![feature(negative_impls)]

use std::{
    fs::{self, File},
    io::Write,
    marker::PhantomData,
    net,
};

pub struct LabelSecretLevelTop {}

pub trait LessOrEqualSecretThan<L> {}
pub trait MoreOrEqualSecretThan<L> {}

impl<L> MoreOrEqualSecretThan<L> for L {}

// L2 <= L1 -> L1 >= L2
impl<L1, L2> LessOrEqualSecretThan<L1> for L2 where L1: MoreOrEqualSecretThan<L2> {}

pub struct SideEffectFreeFn<T1, T2, F: Fn(T1) -> T2> {
    _phantom1: PhantomData<T1>,
    _phantom2: PhantomData<T2>,
    f: F,
}

impl<T1, T2, F: Fn(T1) -> T2> SideEffectFreeFn<T1, T2, F> {
    pub unsafe fn new(f: F) -> Self {
        Self {
            _phantom1: PhantomData,
            _phantom2: PhantomData,
            f,
        }
    }
}

pub struct SecretCell<T, SecretLabel> {
    _phantom: PhantomData<SecretLabel>,
    data: T,
}

impl<T1, L1> SecretCell<T1, L1> {
    pub fn new(data: T1) -> Self {
        Self {
            _phantom: PhantomData,
            data,
        }
    }

    pub fn upgrade<L2: MoreOrEqualSecretThan<L1>>(self) -> SecretCell<T1, L2> {
        SecretCell {
            _phantom: PhantomData,
            data: self.data,
        }
    }

    // Unwrapped data is on the lowest level
    pub fn set(&mut self, value: T1) {
        self.data = value;
    }

    // The cell's value can be taken from cells in lower levels
    pub fn assign<L2: LessOrEqualSecretThan<L1>>(&mut self, other: SecretCell<T1, L2>) {
        self.data = other.data;
    }

    // fmap in Haskell, make it a Functor
    pub fn map<T2, F: Fn(T1) -> T2>(self, f: SideEffectFreeFn<T1, T2, F>) -> SecretCell<T2, L1> {
        SecretCell::new((f.f)(self.data))
    }

    // bind in Haskell, make it a Monad
    pub fn and_then<L2, T2, F>(self, f: F) -> SecretCell<T2, L2>
    where
        L2: MoreOrEqualSecretThan<L1>,
        F: Fn(T1) -> SecretCell<T2, L2>,
    {
        f(self.data)
    }

    // unsecure
    pub unsafe fn declassify(self) -> T1 {
        self.data
    }
}

impl<T1, T2, L1> From<SecretCell<(T1, T2), L1>> for (SecretCell<T1, L1>, SecretCell<T2, L1>) {
    fn from(value: SecretCell<(T1, T2), L1>) -> Self {
        let (v1, v2) = unsafe { value.declassify() };
        (SecretCell::new(v1), SecretCell::new(v2))
    }
}

impl<T1, T2, L1> From<(SecretCell<T1, L1>, SecretCell<T2, L1>)> for SecretCell<(T1, T2), L1> {
    fn from((v1, v2): (SecretCell<T1, L1>, SecretCell<T2, L1>)) -> Self {
        unsafe { SecretCell::new((v1.declassify(), v2.declassify())) }
    }
}

pub unsafe auto trait SideEffectFree {}
impl !SideEffectFree for std::fs::File {}
// impl<T> !SideEffectFree for T where T: Write {}
impl<T> !SideEffectFree for std::cell::UnsafeCell<T> {}
impl !SideEffectFree for net::UdpSocket {}
impl !SideEffectFree for net::TcpStream {}

pub unsafe trait SideEffectChecker {
    fn check(&self) {}
}

unsafe impl<T: SideEffectFree> SideEffectChecker for T {}

#[cfg(test)]
mod tests {
    use std::cell::RefCell;

    use super::*;

    #[test]
    fn it_works() {
        let mut secret_data = SecretCell::<_, LabelSecretLevelTop>::new("Hello".to_string());
        let mut unsecure_data = String::new();
        let unsecure_cell = RefCell::new(unsecure_data);
        let new_secret_data = secret_data.map(unsafe {
            SideEffectFreeFn::new(|sec_data| {
                unsecure_data.check();
                unsecure_cell.check();
                *unsecure_cell.borrow_mut() = sec_data;
            })
        });
        let new_unsecure_data = unsecure_cell.borrow().clone();
    }
}
