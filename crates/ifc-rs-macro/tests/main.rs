use ifc_rs::*;
use ifc_rs_macro::check_side_effect;
use std::cell::RefCell;

fn main() {
    // This **should not** to be compiled successful!
    let mut secret_data = SecretCell::<_, LabelSecretLevelTop>::new("Hello".to_string());
    let mut unsecure_data = String::new();
    let unsecure_cell = RefCell::new(unsecure_data);
    let new_secret_data = secret_data.map(check_side_effect!(|sec_data| {
        *unsecure_cell.borrow_mut() = sec_data;
    }));
}
